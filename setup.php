<?php

require('vendor/autoload.php');

use Aws\S3\S3Client;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use Symfony\Component\Process\ProcessBuilder;

function upload($filename) {
    $name = basename($filename);

    $client = S3Client::factory([
            'credentials' => [
            'key'    => getenv('AWS_KEY'),
            'secret' => getenv('AWS_SECRET'),
        ],
        'region' => 'us-east-1',
        'version' => 'latest',
    ]);

    $adapter = new AwsS3Adapter($client, getenv('AWS_BUCKET') ?: 'awjudd-backup-server', getenv('AWS_FOLDER'));

    $filesystem = new Filesystem($adapter);

    if($filesystem->has($name)) {
        $filesystem->delete($name);
    }

    $stream = fopen($filename, 'r+');

    $filesystem->writeStream($name, $stream);
    fclose($stream);
}